/* @copyright Itential, LLC 2018 */

/* eslint no-underscore-dangle:warn */
/* eslint import/no-dynamic-require: warn */

// Set globals
/* global log */

/* Required libraries.  */
const path = require('path');

/* Fetch in the other needed components for the this Adaptor */
const AdapterBaseCl = require(path.join(__dirname, 'adapterBase.js'));

/**
 * This is the adapter/interface into EAI
 */

/* GENERAL ADAPTER FUNCTIONS */
class EAI extends AdapterBaseCl {
  /**
   * EAI Adapter
   * @constructor
   */
  /* Working on changing the way we do Emit methods due to size and time constrainsts
  constructor(prongid, properties) {
    // Instantiate the AdapterBase super class
    super(prongid, properties);

    const restFunctionNames = this.getWorkflowFunctions();

    // Dynamically bind emit functions
    for (let i = 0; i < restFunctionNames.length; i += 1) {
      // Bind function to have name fnNameEmit for fnName
      const version = restFunctionNames[i].match(/__v[0-9]+/);
      const baseFnName = restFunctionNames[i].replace(/__v[0-9]+/, '');
      const fnNameEmit = version ? `${baseFnName}Emit${version}` : `${baseFnName}Emit`;
      this[fnNameEmit] = function (...args) {
        // extract the callback
        const callback = args[args.length - 1];
        // slice the callback from args so we can insert our own
        const functionArgs = args.slice(0, args.length - 1);
        // create a random name for the listener
        const eventName = `${restFunctionNames[i]}:${Math.random().toString(36)}`;
        // tell the calling class to start listening
        callback({ event: eventName, status: 'received' });
        // store parent for use of this context later
        const parent = this;
        // store emission function
        const func = function (val, err) {
          parent.removeListener(eventName, func);
          parent.emit(eventName, val, err);
        };
        // Use apply to call the function in a specific context
        this[restFunctionNames[i]].apply(this, functionArgs.concat([func])); // eslint-disable-line prefer-spread
      };
    }

    // Uncomment if you have things to add to the constructor like using your own properties.
    // Otherwise the constructor in the adapterBase will be used.
    // Capture my own properties - they need to be defined in propertiesSchema.json
    // if (this.allProps && this.allProps.myownproperty) {
    //   mypropvariable = this.allProps.myownproperty;
    // }
  }
  */

  /**
   * @callback healthCallback
   * @param {Object} reqObj - the request to send into the healthcheck
   * @param {Callback} callback - The results of the call
   */
  healthCheck(reqObj, callback) {
    // you can modify what is passed into the healthcheck by changing things in the newReq
    let newReq = null;
    if (reqObj) {
      newReq = Object.assign(...reqObj);
    }
    super.healthCheck(newReq, callback);
  }

  /**
   * @iapGetAdapterWorkflowFunctions
   */
  iapGetAdapterWorkflowFunctions(inIgnore) {
    let myIgnore = [
      'healthCheck',
      'iapGetAdapterWorkflowFunctions',
      'hasEntities',
      'getAuthorization'
    ];
    if (!inIgnore && Array.isArray(inIgnore)) {
      myIgnore = inIgnore;
    } else if (!inIgnore && typeof inIgnore === 'string') {
      myIgnore = [inIgnore];
    }

    // The generic adapter functions should already be ignored (e.g. healthCheck)
    // you can add specific methods that you do not want to be workflow functions to ignore like below
    // myIgnore.push('myMethodNotInWorkflow');

    return super.iapGetAdapterWorkflowFunctions(myIgnore);
  }

  /**
   * iapUpdateAdapterConfiguration is used to update any of the adapter configuration files. This
   * allows customers to make changes to adapter configuration without having to be on the
   * file system.
   *
   * @function iapUpdateAdapterConfiguration
   * @param {string} configFile - the name of the file being updated (required)
   * @param {Object} changes - an object containing all of the changes = formatted like the configuration file (required)
   * @param {string} entity - the entity to be changed, if an action, schema or mock data file (optional)
   * @param {string} type - the type of entity file to change, (action, schema, mock) (optional)
   * @param {string} action - the action to be changed, if an action, schema or mock data file (optional)
   * @param {boolean} replace - true to replace entire mock data, false to merge/append
   * @param {Callback} callback - The results of the call
   */
  iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, replace, callback) {
    const meth = 'adapter-iapUpdateAdapterConfiguration';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    super.iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, replace, callback);
  }

  /**
    * @summary Suspends adapter
    *
    * @function iapSuspendAdapter
    * @param {Callback} callback - callback function
    */
  iapSuspendAdapter(mode, callback) {
    const meth = 'adapter-iapSuspendAdapter';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapSuspendAdapter(mode, callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
    * @summary Unsuspends adapter
    *
    * @function iapUnsuspendAdapter
    * @param {Callback} callback - callback function
    */
  iapUnsuspendAdapter(callback) {
    const meth = 'adapter-iapUnsuspendAdapter';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapUnsuspendAdapter(callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
    * @summary Get the Adapter Queue
    *
    * @function iapGetAdapterQueue
    * @param {Callback} callback - callback function
    */
  iapGetAdapterQueue(callback) {
    const meth = 'adapter-iapGetAdapterQueue';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    return super.iapGetAdapterQueue(callback);
  }

  /* SCRIPT CALLS */
  /**
   * See if the API path provided is found in this adapter
   *
   * @function iapFindAdapterPath
   * @param {string} apiPath - the api path to check on
   * @param {Callback} callback - The results of the call
   */
  iapFindAdapterPath(apiPath, callback) {
    const meth = 'adapter-iapFindAdapterPath';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    super.iapFindAdapterPath(apiPath, callback);
  }

  /**
  * @summary Runs troubleshoot scripts for adapter
  *
  * @function iapTroubleshootAdapter
  * @param {Object} props - the connection, healthcheck and authentication properties
  *
  * @param {boolean} persistFlag - whether the adapter properties should be updated
  * @param {Callback} callback - The results of the call
  */
  iapTroubleshootAdapter(props, persistFlag, callback) {
    const meth = 'adapter-iapTroubleshootAdapter';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapTroubleshootAdapter(props, persistFlag, this, callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
    * @summary runs healthcheck script for adapter
    *
    * @function iapRunAdapterHealthcheck
    * @param {Adapter} adapter - adapter instance to troubleshoot
    * @param {Callback} callback - callback function
    */
  iapRunAdapterHealthcheck(callback) {
    const meth = 'adapter-iapRunAdapterHealthcheck';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapRunAdapterHealthcheck(this, callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
    * @summary runs connectivity check script for adapter
    *
    * @function iapRunAdapterConnectivity
    * @param {Callback} callback - callback function
    */
  iapRunAdapterConnectivity(callback) {
    const meth = 'adapter-iapRunAdapterConnectivity';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapRunAdapterConnectivity(callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
    * @summary runs basicGet script for adapter
    *
    * @function iapRunAdapterBasicGet
    * @param {Callback} callback - callback function
    */
  iapRunAdapterBasicGet(callback) {
    const meth = 'adapter-iapRunAdapterBasicGet';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapRunAdapterBasicGet(callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
   * @summary moves entites into Mongo DB
   *
   * @function iapMoveAdapterEntitiesToDB
   * @param {getCallback} callback - a callback function to return the result (Generics)
   *                                  or the error
   */
  iapMoveAdapterEntitiesToDB(callback) {
    const meth = 'adapter-iapMoveAdapterEntitiesToDB';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapMoveAdapterEntitiesToDB(callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Deactivate adapter tasks
   *
   * @function iapDeactivateTasks
   *
   * @param {Array} tasks - List of tasks to deactivate
   * @param {Callback} callback
   */
  iapDeactivateTasks(tasks, callback) {
    const meth = 'adapter-iapDeactivateTasks';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapDeactivateTasks(tasks, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Activate adapter tasks that have previously been deactivated
   *
   * @function iapActivateTasks
   *
   * @param {Array} tasks - List of tasks to activate
   * @param {Callback} callback
   */
  iapActivateTasks(tasks, callback) {
    const meth = 'adapter-iapActivateTasks';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapActivateTasks(tasks, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /* CACHE CALLS */
  /**
   * @summary Populate the cache for the given entities
   *
   * @function iapPopulateEntityCache
   * @param {String/Array of Strings} entityType - the entity type(s) to populate
   * @param {Callback} callback - whether the cache was updated or not for each entity type
   *
   * @returns status of the populate
   */
  iapPopulateEntityCache(entityTypes, callback) {
    const meth = 'adapter-iapPopulateEntityCache';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapPopulateEntityCache(entityTypes, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Retrieves data from cache for specified entity type
   *
   * @function iapRetrieveEntitiesCache
   * @param {String} entityType - entity of which to retrieve
   * @param {Object} options - settings of which data to return and how to return it
   * @param {Callback} callback - the data if it was retrieved
   */
  iapRetrieveEntitiesCache(entityType, options, callback) {
    const meth = 'adapter-iapCheckEiapRetrieveEntitiesCachentityCached';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapRetrieveEntitiesCache(entityType, options, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /* BROKER CALLS */
  /**
   * @summary Determines if this adapter supports any in a list of entities
   *
   * @function hasEntities
   * @param {String} entityType - the entity type to check for
   * @param {Array} entityList - the list of entities we are looking for
   *
   * @param {Callback} callback - A map where the entity is the key and the
   *                              value is true or false
   */
  hasEntities(entityType, entityList, callback) {
    const meth = 'adapter-hasEntities';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.hasEntities(entityType, entityList, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Get Appliance that match the deviceName
   *
   * @function getDevice
   * @param {String} deviceName - the deviceName to find (required)
   *
   * @param {getCallback} callback - a callback function to return the result
   *                                 (appliance) or the error
   */
  getDevice(deviceName, callback) {
    const meth = 'adapter-getDevice';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.getDevice(deviceName, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Get Appliances that match the filter
   *
   * @function getDevicesFiltered
   * @param {Object} options - the data to use to filter the appliances (optional)
   *
   * @param {getCallback} callback - a callback function to return the result
   *                                 (appliances) or the error
   */
  getDevicesFiltered(options, callback) {
    const meth = 'adapter-getDevicesFiltered';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.getDevicesFiltered(options, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Gets the status for the provided appliance
   *
   * @function isAlive
   * @param {String} deviceName - the deviceName of the appliance. (required)
   *
   * @param {configCallback} callback - callback function to return the result
   *                                    (appliance isAlive) or the error
   */
  isAlive(deviceName, callback) {
    const meth = 'adapter-isAlive';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.isAlive(deviceName, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Gets a config for the provided Appliance
   *
   * @function getConfig
   * @param {String} deviceName - the deviceName of the appliance. (required)
   * @param {String} format - the desired format of the config. (optional)
   *
   * @param {configCallback} callback - callback function to return the result
   *                                    (appliance config) or the error
   */
  getConfig(deviceName, format, callback) {
    const meth = 'adapter-getConfig';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.getConfig(deviceName, format, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Gets the device count from the system
   *
   * @function iapGetDeviceCount
   *
   * @param {getCallback} callback - callback function to return the result
   *                                    (count) or the error
   */
  iapGetDeviceCount(callback) {
    const meth = 'adapter-iapGetDeviceCount';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapGetDeviceCount(callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /* GENERIC ADAPTER REQUEST - allows extension of adapter without new calls being added */
  /**
   * Makes the requested generic call
   *
   * @function iapExpandedGenericAdapterRequest
   * @param {Object} metadata - metadata for the call (optional).
   *                 Can be a stringified Object.
   * @param {String} uriPath - the path of the api call - do not include the host, port, base path or version (optional)
   * @param {String} restMethod - the rest method (GET, POST, PUT, PATCH, DELETE) (optional)
   * @param {Object} pathVars - the parameters to be put within the url path (optional).
   *                 Can be a stringified Object.
   * @param {Object} queryData - the parameters to be put on the url (optional).
   *                 Can be a stringified Object.
   * @param {Object} requestBody - the body to add to the request (optional).
   *                 Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                 Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result (Generics)
   *                 or the error
   */
  iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback) {
    const meth = 'adapter-iapExpandedGenericAdapterRequest';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * Makes the requested generic call
   *
   * @function genericAdapterRequest
   * @param {String} uriPath - the path of the api call - do not include the host, port, base path or version (required)
   * @param {String} restMethod - the rest method (GET, POST, PUT, PATCH, DELETE) (required)
   * @param {Object} queryData - the parameters to be put on the url (optional).
   *                 Can be a stringified Object.
   * @param {Object} requestBody - the body to add to the request (optional).
   *                 Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                 Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result (Generics)
   *                 or the error
   */
  genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback) {
    const meth = 'adapter-genericAdapterRequest';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * Makes the requested generic call with no base path or version
   *
   * @function genericAdapterRequestNoBasePath
   * @param {String} uriPath - the path of the api call - do not include the host, port, base path or version (required)
   * @param {String} restMethod - the rest method (GET, POST, PUT, PATCH, DELETE) (required)
   * @param {Object} queryData - the parameters to be put on the url (optional).
   *                 Can be a stringified Object.
   * @param {Object} requestBody - the body to add to the request (optional).
   *                 Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                 Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result (Generics)
   *                 or the error
   */
  genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback) {
    const meth = 'adapter-genericAdapterRequestNoBasePath';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /* INVENTORY CALLS */
  /**
   * @summary run the adapter lint script to return the results.
   *
   * @function iapRunAdapterLint
   * @param {Callback} callback - callback function
   */
  iapRunAdapterLint(callback) {
    const meth = 'adapter-iapRunAdapterLint';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    return super.iapRunAdapterLint(callback);
  }

  /**
   * @summary run the adapter test scripts (baseunit and unit) to return the results.
   *    can not run integration as there can be implications with that.
   *
   * @function iapRunAdapterTests
   * @param {Callback} callback - callback function
   */
  iapRunAdapterTests(callback) {
    const meth = 'adapter-iapRunAdapterTests';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    return super.iapRunAdapterTests(callback);
  }

  /**
   * @summary provide inventory information abbout the adapter
   *
   * @function iapGetAdapterInventory
   * @param {Callback} callback - callback function
   */
  iapGetAdapterInventory(callback) {
    const meth = 'adapter-iapGetAdapterInventory';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    return super.iapGetAdapterInventory(callback);
  }

  /**
   * @callback healthCallback
   * @param {Object} result - the result of the get request (contains an id and a status)
   */
  /**
   * @callback getCallback
   * @param {Object} result - the result of the get request (entity/ies)
   * @param {String} error - any error that occurred
   */
  /**
   * @callback createCallback
   * @param {Object} item - the newly created entity
   * @param {String} error - any error that occurred
   */
  /**
   * @callback updateCallback
   * @param {String} status - the status of the update action
   * @param {String} error - any error that occurred
   */
  /**
   * @callback deleteCallback
   * @param {String} status - the status of the delete action
   * @param {String} error - any error that occurred
   */

  /* START ACTION FUNCTIONS */
  /**
   * @summary This method will get cards from EAI based on the criteria provided
   *
   * @function getCards
   * @param {String} cardId - the id of a specific card to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Cards) or the error
   */
  getCards(cardId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getCards';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (cardId) {
        reqObj.uriPathVars = [cardId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('card', 'getCards', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getCards'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!cardId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('cards', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load cards into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method will get card templates from EAI based on the criteria provided
   *
   * @function getCardTemplates
   * @param {String} templateId - the id of a specific template to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Cards) or the error
   */
  getCardTemplates(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getCardTemplates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (templateId) {
        reqObj.uriPathVars = [templateId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('card', 'getCardTemplates', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getCardTemplates'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!templateId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('cardTemplates', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load card templates into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method will create a card in EAI with the information provided
   *
   * @function createCard
   * @param {Object} cardData - the card to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Card) or the error
   */
  createCard(cardData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createCard';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the card data as it is required.
      if (cardData === undefined || cardData === null || cardData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['cardData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: cardData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('card', 'createCard', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createCard'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method will create a card template in EAI with the information provided
   *
   * @function createCardTemplate
   * @param {Object} templateData - the template to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Card) or the error
   */
  createCardTemplate(templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createCardTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the templateData data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('card', 'createCardTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createCardTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method will update a card in EAI with the information provided
   *
   * @function updateCard
   * @param {String} cardId - the id of the card to update in EAI
   * @param {Object} cardData - the card data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updateCard(cardId, cardData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updateCard';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the card id as it is required.
      if (cardId === undefined || cardId === null || cardId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['cardId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the card data as it is required.
      if (cardData === undefined || cardData === null || cardData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['cardData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: cardData,
        uriPathVars: [cardId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('card', 'updateCard', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateCard'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method will update a card template in EAI with the information provided
   *
   * @function updateCardTemplate
   * @param {String} templateId - the id of the template to update in EAI
   * @param {Object} templateData - the template data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updateCardTemplate(templateId, templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updateCardTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the template data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('card', 'updateCardTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateCardTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method will delete a card in EAI
   *
   * @function deleteCard
   * @param {String} cardId - the id of the card to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deleteCard(cardId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deleteCard';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the card id as it is required.
      if (cardId === undefined || cardId === null || cardId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['cardId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [cardId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('card', 'deleteCard', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteCard'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method will delete a card template in EAI
   *
   * @function deleteCardTemplate
   * @param {String} templateId - the id of the template to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deleteCardTemplate(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deleteCardTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('card', 'deleteCardTemplate', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteCardTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets Containers from EAI based on the criteria provided
   *
   * @function getContainers
   * @param {String} containerId - the id of a specific container to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Containers) or the error
   */
  getContainers(containerId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getContainers';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (containerId) {
        reqObj.uriPathVars = [containerId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('container', 'getContainers', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getContainers'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!containerId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('containers', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load containers into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets Container Templates from EAI based on the criteria provided
   *
   * @function getContainerTemplates
   * @param {String} templateId - the id of a specific template to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Containers) or the error
   */
  getContainerTemplates(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getContainerTemplates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (templateId) {
        reqObj.uriPathVars = [templateId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('container', 'getContainerTemplates', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getContainerTemplates'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!templateId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('containerTemplates', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load container templates into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method creates a Container in EAI with the provided information
   *
   * @function createContainer
   * @param {Object} containerData - the container to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Container) or the error
   */
  createContainer(containerData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createContainer';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the container data as it is required.
      if (containerData === undefined || containerData === null || containerData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['containerData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: containerData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('container', 'createContainer', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createContainer'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method creates a Container Template in EAI with the provided information
   *
   * @function createContainerTemplate
   * @param {Object} templateData - the template to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Container) or the error
   */
  createContainerTemplate(templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createContainerTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('container', 'createContainerTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createContainerTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method updates a Container in EAI with the provided information
   *
   * @function updateContainer
   * @param {String} containerId - the id of the container to update in EAI
   * @param {Object} containerData - the container data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updateContainer(containerId, containerData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updateContainer';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the container id as it is required.
      if (containerId === undefined || containerId === null || containerId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['containerId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the container data as it is required.
      if (containerData === undefined || containerData === null || containerData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['containerData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: containerData,
        uriPathVars: [containerId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('container', 'updateContainer', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateContainer'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method updates a Container Template in EAI with the provided information
   *
   * @function updateContainerTemplate
   * @param {String} templateId - the id of the template to update in EAI
   * @param {Object} templateData - the template data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updateContainerTemplate(templateId, templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updateContainerTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the template data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('container', 'updateContainerTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateContainerTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method deletes a Container from EAI
   *
   * @function deleteContainer
   * @param {String} containerId - the id of the container to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deleteContainer(containerId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deleteContainer';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the container id as it is required.
      if (containerId === undefined || containerId === null || containerId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['containerId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [containerId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('container', 'deleteContainer', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteContainer'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method deletes a Container Template from EAI
   *
   * @function deleteContainerTemplate
   * @param {String} templateId - the id of the template to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deleteContainerTemplate(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deleteContainerTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('container', 'deleteContainerTemplate', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteContainerTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets equipment from EAI based on the critieria provided
   *
   * @function getEquipments
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Equipments) or the error
   */
  getEquipments(queryParams, addlHeaders, callback) {
    const meth = 'adapter-getEquipments';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('equipment', 'getEquipments', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getEquipments'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets Ports from EAI based on the criteria provided
   *
   * @function getPorts
   * @param {String} portId - the id of a specific port to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Ports) or the error
   */
  getPorts(portId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getPorts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (portId) {
        reqObj.uriPathVars = [portId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('port', 'getPorts', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getPorts'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!portId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('ports', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load ports into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets Port Templates from EAI based on the criteria provided
   *
   * @function getPortTemplates
   * @param {String} templateId - the id of a specific port template to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Ports) or the error
   */
  getPortTemplates(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getPortTemplates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (templateId) {
        reqObj.uriPathVars = [templateId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('port', 'getPortTemplates', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getPortTemplates'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!templateId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('porttemplates', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load ports into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method creates a port in EAI with the provided information
   *
   * @function createPort
   * @param {Object} portData - the port to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Port) or the error
   */
  createPort(portData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createPort';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the port data as it is required.
      if (portData === undefined || portData === null || portData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['portData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: portData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('port', 'createPort', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createPort'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method creates a port template in EAI with the provided information
   *
   * @function createPortTemplate
   * @param {Object} templateData - the port template to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Port) or the error
   */
  createPortTemplate(templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createPortTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the port data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('port', 'createPortTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createPortTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method updates a Port in EAI with the information provided
   *
   * @function updatePort
   * @param {String} portId - the id of the port to update in EAI
   * @param {Object} portData - the port data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updatePort(portId, portData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updatePort';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the port id as it is required.
      if (portId === undefined || portId === null || portId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['portId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the port data as it is required.
      if (portData === undefined || portData === null || portData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['portData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: portData,
        uriPathVars: [portId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('port', 'updatePort', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updatePort'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method updates a Port Template in EAI with the information provided
   *
   * @function updatePortTemplate
   * @param {String} templateId - the id of the port template to update in EAI
   * @param {Object} templateData - the port template data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updatePortTemplate(templateId, templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updatePortTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the port id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the port data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('port', 'updatePortTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updatePortTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method deletes a Port from EAI
   *
   * @function deletePort
   * @param {String} portId - the id of the port to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deletePort(portId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deletePort';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the port id as it is required.
      if (portId === undefined || portId === null || portId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['portId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [portId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('port', 'deletePort', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deletePort'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method deletes a Port Template from EAI
   *
   * @function deletePortTemplate
   * @param {String} templateId - the id of the port template to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deletePortTemplate(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deletePortTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the port id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('port', 'deletePortTemplate', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deletePortTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets shelves from EAI with the criteria provided
   *
   * @function getShelves
   * @param {String} shelfId - the id of a specific shelf to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Shelves) or the error
   */
  getShelves(shelfId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getShelves';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (shelfId) {
        reqObj.uriPathVars = [shelfId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('shelf', 'getShelves', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getShelves'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!shelfId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('shelves', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load shelves into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets shelf Templates from EAI with the criteria provided
   *
   * @function getShelfTemplates
   * @param {String} templateId - the id of a specific template to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Shelves) or the error
   */
  getShelfTemplates(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getShelfTemplates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (templateId) {
        reqObj.uriPathVars = [templateId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('shelf', 'getShelfTemplates', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getShelfTemplates'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!templateId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('shelfTemplates', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load shelf templates into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method creates a Shelf in EAI with the provided information
   *
   * @function createShelf
   * @param {Object} shelfData - the shelf to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Shelf) or the error
   */
  createShelf(shelfData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createShelf';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the shelf data as it is required.
      if (shelfData === undefined || shelfData === null || shelfData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['shelfData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: shelfData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('shelf', 'createShelf', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createShelf'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method creates a Shelf Template in EAI with the provided information
   *
   * @function createShelfTemplate
   * @param {Object} templateData - the template to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Shelf) or the error
   */
  createShelfTemplate(templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createShelfTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('shelf', 'createShelfTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createShelfTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method updates a Shelf in EAI with the provided information
   *
   * @function updateShelf
   * @param {String} shelfId - the id of the shelf to update in EAI
   * @param {Object} shelfData - the shelf data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updateShelf(shelfId, shelfData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updateShelf';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the shelf id as it is required.
      if (shelfId === undefined || shelfId === null || shelfId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['shelfId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the shelf data as it is required.
      if (shelfData === undefined || shelfData === null || shelfData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['shelfData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: shelfData,
        uriPathVars: [shelfId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('shelf', 'updateShelf', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateShelf'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method updates a Shelf Template in EAI with the provided information
   *
   * @function updateShelfTemplate
   * @param {String} templateId - the id of the template to update in EAI
   * @param {Object} templateData - the template data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updateShelfTemplate(templateId, templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updateShelfTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the template data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('shelf', 'updateShelfTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateShelfTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method deletes a Shelf in EAI
   *
   * @function deleteShelf
   * @param {String} shelfId - the id of the shelf to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deleteShelf(shelfId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deleteShelf';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the shelf id as it is required.
      if (shelfId === undefined || shelfId === null || shelfId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['shelfId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [shelfId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('shelf', 'deleteShelf', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteShelf'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method deletes a Shelf Template in EAI
   *
   * @function deleteShelfTemplate
   * @param {String} templateId - the id of the template to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deleteShelfTemplate(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deleteShelfTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('shelf', 'deleteShelfTemplate', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteShelfTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets Sites from EAI with the criteria provided
   *
   * @function getSites
   * @param {String} siteId - the id of a specific site to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Sites) or the error
   */
  getSites(siteId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getSites';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (siteId) {
        reqObj.uriPathVars = [siteId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('site', 'getSites', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getSites'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!siteId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('sites', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load sites into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets Site Templates from EAI with the criteria provided
   *
   * @function getSiteTemplates
   * @param {String} templateId - the id of a specific site template to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Sites) or the error
   */
  getSiteTemplates(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getSiteTemplates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (templateId) {
        reqObj.uriPathVars = [templateId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('site', 'getSiteTemplates', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getSiteTemplates'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!templateId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('sitetemplates', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load site templates into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method creates a Site in EAI with the provided information
   *
   * @function createSite
   * @param {Object} siteData - the site to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Site) or the error
   */
  createSite(siteData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createSite';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the site data as it is required.
      if (siteData === undefined || siteData === null || siteData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['siteData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: siteData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('site', 'createSite', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createSite'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method creates a Site Template in EAI with the provided information
   *
   * @function createSiteTemplate
   * @param {Object} templateData - the site template to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Site) or the error
   */
  createSiteTemplate(templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createSiteTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the site data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('site', 'createSiteTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createSiteTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method updates a Site in EAI with the provided information
   *
   * @function updateSite
   * @param {String} siteId - the id of the site to update in EAI
   * @param {Object} siteData - the site data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updateSite(siteId, siteData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updateSite';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the site id as it is required.
      if (siteId === undefined || siteId === null || siteId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['siteId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the site data as it is required.
      if (siteData === undefined || siteData === null || siteData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['siteData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: siteData,
        uriPathVars: [siteId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('site', 'updateSite', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateSite'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method updates a Site Template in EAI with the provided information
   *
   * @function updateSiteTemplate
   * @param {String} templateId - the id of the site template to update in EAI
   * @param {Object} templateData - the site template data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updateSiteTemplate(templateId, templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updateSiteTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the site id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the site data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('site', 'updateSiteTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateSiteTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method deletes a Site in EAI
   *
   * @function deleteSite
   * @param {String} siteId - the id of the site to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deleteSite(siteId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deleteSite';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the site id as it is required.
      if (siteId === undefined || siteId === null || siteId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['siteId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [siteId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('site', 'deleteSite', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteSite'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method deletes a Site Template in EAI
   *
   * @function deleteSiteTemplate
   * @param {String} templateId - the id of the site template to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deleteSiteTemplate(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deleteSiteTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the site id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('site', 'deleteSiteTemplate', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteSiteTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets Slots from EAI with the criteria provided
   *
   * @function getSlots
   * @param {String} slotId - the id of a specific slot to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Slots) or the error
   */
  getSlots(slotId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getSlots';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (slotId) {
        reqObj.uriPathVars = [slotId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('slot', 'getSlots', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getSlots'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!slotId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('slots', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load slots into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets Slot Templates from EAI with the criteria provided
   *
   * @function getSlotTemplates
   * @param {String} templateId - the id of a specific template to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Slots) or the error
   */
  getSlotTemplates(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getSlotTemplates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (templateId) {
        reqObj.uriPathVars = [templateId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('slot', 'getSlotTemplates', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getSlotTemplates'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!templateId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('slotTemplates', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load slot templates into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method creates a Slot in EAI with the provided information
   *
   * @function createSlot
   * @param {Object} slotData - the slot to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Slot) or the error
   */
  createSlot(slotData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createSlot';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the slot data as it is required.
      if (slotData === undefined || slotData === null || slotData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['slotData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: slotData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('slot', 'createSlot', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createSlot'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method creates a Slot Template in EAI with the provided information
   *
   * @function createSlotTemplate
   * @param {Object} templateData - the template to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Slot) or the error
   */
  createSlotTemplate(templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createSlotTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('slot', 'createSlotTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createSlotTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method updates a Slot in EAI with the provided information
   *
   * @function updateSlot
   * @param {String} slotId - the id of the slot to update in EAI
   * @param {Object} slotData - the slot data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updateSlot(slotId, slotData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updateSlot';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the slot id as it is required.
      if (slotId === undefined || slotId === null || slotId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['slotId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the slot data as it is required.
      if (slotData === undefined || slotData === null || slotData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['slotData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: slotData,
        uriPathVars: [slotId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('slot', 'updateSlot', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateSlot'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method updates a Slot Template in EAI with the provided information
   *
   * @function updateSlotTemplate
   * @param {String} templateId - the id of the template to update in EAI
   * @param {Object} templateData - the template data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updateSlotTemplate(templateId, templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updateSlotTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the template data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('slot', 'updateSlotTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateSlotTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method deletes a Slot from EAI
   *
   * @function deleteSlot
   * @param {String} slotId - the id of the slot to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deleteSlot(slotId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deleteSlot';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the slot id as it is required.
      if (slotId === undefined || slotId === null || slotId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['slotId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [slotId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('slot', 'deleteSlot', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteSlot'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method deletes a Slot Template from EAI
   *
   * @function deleteSlotTemplate
   * @param {String} templateId - the id of the template to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deleteSlotTemplate(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deleteSlotTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('slot', 'deleteSlotTemplate', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteSlotTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets Trails from EAI with the criteria provided
   *
   * @function getTrails
   * @param {String} trailId - the id of a specific trail to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Trails) or the error
   */
  getTrails(trailId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getTrails';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (trailId) {
        reqObj.uriPathVars = [trailId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('trail', 'getTrails', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getTrails'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!trailId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('trails', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load trails into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets Trails from EAI with the criteria provided
   *
   * @function getTrailQuery
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Trails) or the error
   */
  getTrailQuery(queryParams, addlHeaders, callback) {
    const meth = 'adapter-getTrailQuery';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('trail', 'getTrailQuery', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getTrailQuery'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method gets Trail Templates from EAI with the criteria provided
   *
   * @function getTrailTemplates
   * @param {String} templateId - the id of a specific template to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result
   *                                 (Trails) or the error
   */
  getTrailTemplates(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getTrailTemplates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        addlHeaders
      };

      // add path vars if provided
      if (templateId) {
        reqObj.uriPathVars = [templateId];
      }
      // add query if provided
      if (queryParams) {
        reqObj.uriQuery = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('trail', 'getTrailTemplates', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getTrailTemplates'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if no error and a get ALL, add the entities to the cache
        if (!templateId && this.caching) {
          // add the entities to the cache - can run on its own
          this.addEntityCache('trailTemplates', result.response, 'id', (loaded, err) => {
            if (!loaded) {
              log.trace(`${origin}: Could not load trail templates into cache - ${err}`);
            }
          });
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method creates a Trail in EAI with the provided infromation
   *
   * @function createTrail
   * @param {Object} trailData - the trail to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Trail) or the error
   */
  createTrail(trailData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createTrail';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the trail data as it is required.
      if (trailData === undefined || trailData === null || trailData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['trailData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: trailData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('trail', 'createTrail', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createTrail'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method creates a Trail Template in EAI with the provided infromation
   *
   * @function createTrailTemplate
   * @param {Object} templateData - the template to create in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                                    (Trail) or the error
   */
  createTrailTemplate(templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-createTrailTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('trail', 'createTrailTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createTrailTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method updates a Trail in EAI with the provided information
   *
   * @function updateTrail
   * @param {String} trailId - the id of the trail to update in EAI
   * @param {Object} trailData - the trail data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updateTrail(trailId, trailData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updateTrail';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the trail id as it is required.
      if (trailId === undefined || trailId === null || trailId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['trailId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the trail data as it is required.
      if (trailData === undefined || trailData === null || trailData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['trailData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: trailData,
        uriPathVars: [trailId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('trail', 'updateTrail', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateTrail'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method updates a Trail based on the provided site in EAI with the provided information
   *
   * @function updateTrailBySite
   * @param {String} siteQuery - the query to find the trail site to update in EAI
   * @param {Object} trailData - the trail data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updateTrailBySite(siteQuery, trailData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updateTrailBySite';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the trail id as it is required.
      if (siteQuery === undefined || siteQuery === null || siteQuery === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['siteQuery'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the trail data as it is required.
      if (trailData === undefined || trailData === null || trailData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['trailData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: trailData,
        uriQuery: siteQuery,
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('trail', 'updateTrailBySite', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateTrailBySite'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method updates a Trail Template in EAI with the provided information
   *
   * @function updateTrailTemplate
   * @param {String} templateId - the id of the template to update in EAI
   * @param {Object} templateData - the template data to update in EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  updateTrailTemplate(templateId, templateData, queryParams, addlHeaders, callback) {
    const meth = 'adapter-updateTrailTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      // verify the template data as it is required.
      if (templateData === undefined || templateData === null || templateData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: templateData,
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('trail', 'updateTrailTemplate', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateTrailTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method deletes a Trail from EAI
   *
   * @function deleteTrail
   * @param {String} trailId - the id of the trail to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deleteTrail(trailId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deleteTrail';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the trail id as it is required.
      if (trailId === undefined || trailId === null || trailId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['trailId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [trailId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('trail', 'deleteTrail', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteTrail'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This method deletes a Trail Template from EAI
   *
   * @function deleteTrailTemplate
   * @param {String} templateId - the id of the template to remove from EAI
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                               Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                                    (status of the request) or the error
   */
  deleteTrailTemplate(templateId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-deleteTrailTemplate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (templateId === undefined || templateId === null || templateId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['templateId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: [templateId],
        addlHeaders
      };
      // add query if provided
      if (queryParams) {
        reqObj.uriOptions = queryParams;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('trail', 'deleteTrailTemplate', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteTrailTemplate'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Gets the generics from EAI
   *
   * Call to get the generics from EAI. If a entityId is provided it will retrieve the
   * specific generic. Otherwise it will retrieve all the generics.
   *
   * @function getGenerics
   * @param {String} uriPath - the path of the eai data to retrieve (required)
   * @param {String} entityId - the id of a specific generic to get. (optional)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                 Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result (Generics)
   *                 or the error
   */
  getGenerics(uriPath, entityId, queryParams, addlHeaders, callback) {
    const meth = 'adapter-getGenerics';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    let returnAll = false;

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (uriPath === undefined || uriPath === null || uriPath === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['uriPath'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // remove any leading / and split the uripath into path variables
      let myPath = uriPath;
      while (myPath.indexOf('/') === 0) {
        myPath = myPath.substring(1);
      }
      const pathVars = myPath.split('/');

      // if id was provided use it
      if (entityId !== null && entityId !== '') {
        pathVars.push(entityId);
      }

      // if the additional headers was passed in as a string parse the json into an object
      let thisAHdata = addlHeaders;
      if (addlHeaders !== null && addlHeaders.constructor === String) {
        try {
          // parse the additional headers object that was passed in
          thisAHdata = JSON.parse(addlHeaders);
        } catch (err) {
          log.warn(`An error occurred parsing additional headers: ${err}`);
          thisAHdata = null;
        }
      }

      // if the query parameters was passed in as a string parse the json into an object
      let thisQdata = queryParams;
      if (queryParams !== null && queryParams.constructor === String) {
        try {
          // parse the query parameters object that was passed in
          thisQdata = JSON.parse(queryParams);
        } catch (err) {
          log.warn(`An error occurred parsing query parameters: ${err}`);
          thisQdata = null;
        }
      }
      if (thisQdata && thisQdata.returnFull !== undefined && thisQdata.returnFull !== null) {
        returnAll = thisQdata.returnFull;
        delete thisQdata.returnFull;
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: pathVars,
        addlHeaders: thisAHdata
      };

      // add query if provided
      if (thisQdata) {
        reqObj.uriQuery = thisQdata;
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('generic', 'getGenerics', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          // return the response
          if (!returnAll && error.response) {
            return callback(null, error.response);
          }
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getGenerics'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        if (returnAll) {
          return callback(result);
        }
        return callback(result.response);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to create a generic in EAI.
   *
   * The function will take the provided Pronghorn Generic and create it in EAI
   *
   * @function createGeneric
   * @param {String} uriPath - the path of the eai data to retrieve (required)
   * @param {Object} genericData - the generic to create in EAI
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                 Can be a stringified Object.
   * @param {createCallback} callback - a callback function to return the result
   *                  (Generic) or the error
   */
  createGeneric(uriPath, genericData, addlHeaders, callback) {
    const meth = 'adapter-createGeneric';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    let returnAll = false;

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (uriPath === undefined || uriPath === null || uriPath === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['uriPath'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (genericData === undefined || genericData === null || genericData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['genericData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // remove any leading / and split the uripath into path variables
      let myPath = uriPath;
      while (myPath.indexOf('/') === 0) {
        myPath = myPath.substring(1);
      }
      const pathVars = myPath.split('/');

      // if the additional headers was passed in as a string parse the json into an object
      let thisAHdata = addlHeaders;
      if (addlHeaders !== null && addlHeaders.constructor === String) {
        try {
          // parse the additional headers object that was passed in
          thisAHdata = JSON.parse(addlHeaders);
        } catch (err) {
          log.warn(`An error occurred parsing additional headers: ${err}`);
          thisAHdata = null;
        }
      }

      // if the query parameters was passed in as a string parse the json into an object
      let thisBdata = genericData;
      if (genericData !== null && genericData.constructor === String) {
        try {
          // parse the query parameters object that was passed in
          thisBdata = JSON.parse(genericData);
        } catch (err) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['genericData'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }
      if (thisBdata && thisBdata.returnFull !== undefined && thisBdata.returnFull !== null) {
        returnAll = thisBdata.returnFull;
        delete thisBdata.returnFull;
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: thisBdata,
        uriPathVars: pathVars,
        addlHeaders: thisAHdata
      };

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('generic', 'createGeneric', reqObj, true, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          // return the response
          if (!returnAll && error.response) {
            return callback(null, error.response);
          }
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createGeneric'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        if (returnAll) {
          return callback(result);
        }
        return callback(result.response);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Update a generic in EAI
   *
   * The function will take the provided Pronghorn Generic and update it in EAI
   *
   * @function updateGeneric
   * @param {String} uriPath - the path of the entity to update data in (required)
   * @param {String} entityId - the id of the generic to update in EAI
   * @param {Object} genericData - the generic data to update in EAI
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                 Can be a stringified Object.
   * @param {updateCallback} callback - a callback function to return the result
   *                  (status of the request) or the error
   */
  updateGeneric(uriPath, entityId, genericData, addlHeaders, callback) {
    const meth = 'adapter-updateGeneric';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    let returnAll = false;

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (uriPath === undefined || uriPath === null || uriPath === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['uriPath'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (entityId === undefined || entityId === null || entityId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['entityId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (genericData === undefined || genericData === null || genericData === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['genericData'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // remove any leading / and split the uripath into path variables
      let myPath = uriPath;
      while (myPath.indexOf('/') === 0) {
        myPath = myPath.substring(1);
      }
      const pathVars = myPath.split('/');
      pathVars.push(entityId);

      // if the additional headers was passed in as a string parse the json into an object
      let thisAHdata = addlHeaders;
      if (addlHeaders !== null && addlHeaders.constructor === String) {
        try {
          // parse the additional headers object that was passed in
          thisAHdata = JSON.parse(addlHeaders);
        } catch (err) {
          log.warn(`An error occurred parsing additional headers: ${err}`);
          thisAHdata = null;
        }
      }

      // if the query parameters was passed in as a string parse the json into an object
      let thisBdata = genericData;
      if (genericData !== null && genericData.constructor === String) {
        try {
          // parse the query parameters object that was passed in
          thisBdata = JSON.parse(genericData);
        } catch (err) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['genericData'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }
      if (thisBdata && thisBdata.returnFull !== undefined && thisBdata.returnFull !== null) {
        returnAll = thisBdata.returnFull;
        delete thisBdata.returnFull;
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        payload: thisBdata,
        uriPathVars: pathVars,
        addlHeaders: thisAHdata
      };

      // return the response
      if (returnAll) {
        // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
        return this.requestHandlerInst.identifyRequest('generic', 'updateGeneric', reqObj, true, (result, error) => {
          // if we received an error or their is no response on the results return an error
          if (error) {
            return callback(null, error);
          }
          if (!Object.hasOwnProperty.call(result, 'response')) {
            const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateGeneric'], null, null, null);
            log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
            return callback(null, errorObj);
          }

          // return the response
          return callback(result);
        });
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('generic', 'updateGeneric', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateGeneric'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Delete a generic from EAI
   *
   * The function will take the provided Pronghorn Generic and remove it from EAI
   *
   * @function deleteGeneric
   * @param {String} uriPath - the path of the entity to delete (required)
   * @param {String} entityId - the id of the generic to remove from EAI
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                 Can be a stringified Object.
   * @param {deleteCallback} callback - a callback function to return the result
   *                  (status of the request) or the error
   */
  deleteGeneric(uriPath, entityId, addlHeaders, callback) {
    const meth = 'adapter-deleteGeneric';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    try {
      // verify the template id as it is required.
      if (uriPath === undefined || uriPath === null || uriPath === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['uriPath'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (entityId === undefined || entityId === null || entityId === '') {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['entityId'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // remove any leading / and split the uripath into path variables
      let myPath = uriPath;
      while (myPath.indexOf('/') === 0) {
        myPath = myPath.substring(1);
      }
      const pathVars = myPath.split('/');
      pathVars.push(entityId);

      // if the additional headers was passed in as a string parse the json into an object
      let thisAHdata = addlHeaders;
      if (addlHeaders !== null && addlHeaders.constructor === String) {
        try {
          // parse the additional headers object that was passed in
          thisAHdata = JSON.parse(addlHeaders);
        } catch (err) {
          log.warn(`An error occurred parsing additional headers: ${err}`);
          thisAHdata = null;
        }
      }

      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        uriPathVars: pathVars,
        addlHeaders: thisAHdata
      };

      // trail does not support cascade delete option
      let myAction = 'deleteGeneric';
      if (uriPath.includes('trail') && uriPath.includes('template')) {
        myAction = 'deleteGenericNoCascade';
      }

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('generic', myAction, reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteGeneric'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Generic request handler for EAI requests. Will take in the entity (path), an action
   * (GET, POST, PUT, DELETE) and parameters needed to contstruct the right type of API request
   * to send to EAI. It will then process the request through the existing entities and return
   * the results.
   *
   * @function processRequest
   * @param {String} entity - the path for the request (required)
   * @param {String} action - the action for the request (required)
   * @param {Object} queryParams - the parameters to be put on the url (optional).
   *                               Can be a stringified Object.
   * @param {Object} bodyParams - the paramters to be put in the body of the request
   *                              (required for POST and PUT). Can be a stringified Object.
   * @param {Object} addlHeaders - other information to be sent with the request (optional).
   *                               Can be a stringified Object.
   * @param {processCallback} callback - a callback function to return the result or the error
   */
  processRequest(entity, action, queryParams, bodyParams, addlHeaders, callback) {
    const meth = 'adapter-processRequest';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    let returnAll = false;

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (queryParams && queryParams.returnFull !== undefined && queryParams.returnFull !== null) {
      returnAll = queryParams.returnFull;
    }

    try {
      // verify the required data was received
      if (!entity) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['entity'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!action) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['action'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // prepare the query parameters we received
      let thisQdata = queryParams;

      // if the query parameters was passed in as a string parse the json into an object
      if (queryParams !== null && queryParams.constructor === String) {
        try {
          // parse the query parameters object that was passed in
          thisQdata = JSON.parse(queryParams);
        } catch (err) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Query Not Translated', null, null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }
      if (thisQdata && thisQdata.returnFull !== undefined && thisQdata.returnFull !== null) {
        delete thisQdata.returnFull;
      }

      // prepare the body parameters we received
      let thisBdata = bodyParams;

      // if the body parameters was passed in as a string parse the json into an object
      if (bodyParams !== null && bodyParams.constructor === String) {
        try {
          // parse the body parameters object that was passed in
          thisBdata = JSON.parse(bodyParams);
        } catch (err) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Payload Not Translated', null, null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      // handle the right entity and action
      switch (action) {
        case 'fetchByKey': {
          // verify the conditionally required data within the objects was received
          if (!thisQdata || !thisQdata.key) {
            const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['queryParams with key'], null, null, null);
            log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
            return callback(null, errorObj);
          }

          const itemId = thisQdata.key;
          delete thisQdata.key;

          switch (entity) {
            case 'card': {
              if (returnAll) {
                return this.getCards(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getCards(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'cardTemplate': {
              if (returnAll) {
                return this.getCardTemplates(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getCardTemplates(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'container': {
              if (returnAll) {
                return this.getContainers(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getContainers(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'containerTemplate': {
              if (returnAll) {
                return this.getContainerTemplates(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getContainerTemplates(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'port': {
              if (returnAll) {
                return this.getPorts(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getPorts(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'portTemplate': {
              if (returnAll) {
                return this.getPortTemplates(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getPortTemplates(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'shelf': {
              if (returnAll) {
                return this.getShelves(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getShelves(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'shelfTemplate': {
              if (returnAll) {
                return this.getShelfTemplates(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getShelfTemplates(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'site': {
              if (returnAll) {
                return this.getSites(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getSites(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'siteTemplate': {
              if (returnAll) {
                return this.getSiteTemplates(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getSiteTemplates(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'slot': {
              if (returnAll) {
                return this.getSlots(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getSlots(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'slotTemplate': {
              if (returnAll) {
                return this.getSlotTemplates(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getSlotTemplates(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'trail': {
              if (returnAll) {
                return this.getTrails(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getTrails(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'trailTemplate': {
              if (returnAll) {
                return this.getTrailTemplates(itemId, thisQdata, addlHeaders, callback);
              }
              return this.getTrailTemplates(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            default: {
              // unsupported entity type
              const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Entity', [entity], null, null, null);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }
          }
        }
        case 'query': {
          switch (entity) {
            case 'card': {
              if (returnAll) {
                return this.getCards(null, thisQdata, addlHeaders, callback);
              }
              return this.getCards(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'cardTemplate': {
              if (returnAll) {
                return this.getCardTemplates(null, thisQdata, addlHeaders, callback);
              }
              return this.getCardTemplates(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'container': {
              if (returnAll) {
                return this.getContainers(null, thisQdata, addlHeaders, callback);
              }
              return this.getContainers(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'containerTemplate': {
              if (returnAll) {
                return this.getContainerTemplates(null, thisQdata, addlHeaders, callback);
              }
              return this.getContainerTemplates(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'equipment': {
              if (returnAll) {
                return this.getEquipments(null, thisQdata, addlHeaders, callback);
              }
              return this.getEquipments(thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'port': {
              if (returnAll) {
                return this.getPorts(null, thisQdata, addlHeaders, callback);
              }
              return this.getPorts(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'portTemplate': {
              if (returnAll) {
                return this.getPortTemplates(null, thisQdata, addlHeaders, callback);
              }
              return this.getPortTemplates(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'shelf': {
              if (returnAll) {
                return this.getShelves(null, thisQdata, addlHeaders, callback);
              }
              return this.getShelves(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'shelfTemplate': {
              if (returnAll) {
                return this.getShelfTemplates(null, thisQdata, addlHeaders, callback);
              }
              return this.getShelfTemplates(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'site': {
              if (returnAll) {
                return this.getSites(null, thisQdata, addlHeaders, callback);
              }
              return this.getSites(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'siteTemplate': {
              if (returnAll) {
                return this.getSiteTemplate(null, thisQdata, addlHeaders, callback);
              }
              return this.getSiteTemplate(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'slot': {
              if (returnAll) {
                return this.getSlots(null, thisQdata, addlHeaders, callback);
              }
              return this.getSlots(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'slotTemplate': {
              if (returnAll) {
                return this.getSlotTemplates(null, thisQdata, addlHeaders, callback);
              }
              return this.getSlotTemplates(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'trail': {
              if (returnAll) {
                return this.getTrails(null, thisQdata, addlHeaders, callback);
              }
              return this.getTrails(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'trailTemplate': {
              if (returnAll) {
                return this.getTrailTemplates(null, thisQdata, addlHeaders, callback);
              }
              return this.getTrailTemplates(null, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            default: {
              // unsupported entity type
              const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Entity', [entity], null, null, null);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }
          }
        }
        case 'create': {
          // verify the conditionally required data within the objects was received
          if (!thisBdata) {
            const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['bodyParams'], null, null, null);
            log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
            return callback(null, errorObj);
          }

          switch (entity) {
            case 'card': {
              if (thisBdata.type) {
                thisBdata.cardType = thisBdata.type;
              }
              thisBdata.type = 'oci/card';
              if (returnAll) {
                return this.createCard(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createCard(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'cardTemplate': {
              if (thisBdata.type) {
                thisBdata.cardTemplateType = thisBdata.type;
              }
              thisBdata.type = 'oci/cardtemplate';
              if (returnAll) {
                return this.createCardTemplate(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createCardTemplate(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'container': {
              if (thisBdata.type) {
                thisBdata.containerType = thisBdata.type;
              }
              thisBdata.type = 'oci/container';
              if (returnAll) {
                return this.createContainer(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createContainer(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'containerTemplate': {
              if (thisBdata.type) {
                thisBdata.containerTemplateType = thisBdata.type;
              }
              thisBdata.type = 'oci/containertemplate';
              if (returnAll) {
                return this.createContainerTemplate(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createContainerTemplate(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'port': {
              if (thisBdata.type) {
                thisBdata._type = thisBdata.type;
              }
              thisBdata.type = 'oci/port';
              if (returnAll) {
                return this.createPort(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createPort(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'portTemplate': {
              if (thisBdata.type) {
                thisBdata._type = thisBdata.type;
              }
              thisBdata.type = 'oci/porttemplate';
              if (returnAll) {
                return this.createPortTemplate(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createPortTemplate(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'shelf': {
              if (thisBdata.type) {
                thisBdata.shelfType = thisBdata.type;
              }
              thisBdata.type = 'oci/shelf';
              if (returnAll) {
                return this.createShelf(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createShelf(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'shelfTemplate': {
              if (thisBdata.type) {
                thisBdata.shelfTemplateType = thisBdata.type;
              }
              thisBdata.type = 'oci/shelftemplate';
              if (returnAll) {
                return this.createShelfTemplate(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createShelfTemplate(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'site': {
              if (thisBdata.type) {
                thisBdata.siteType = thisBdata.type;
              }
              thisBdata.type = 'oci/site';
              if (returnAll) {
                return this.createSite(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createSite(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'siteTemplate': {
              if (thisBdata.type) {
                thisBdata.siteTemplateType = thisBdata.type;
              }
              thisBdata.type = 'oci/sitetemplate';
              if (returnAll) {
                return this.createSiteTemplate(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createSiteTemplate(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'slot': {
              if (thisBdata.type) {
                thisBdata._type = thisBdata.type;
              }
              thisBdata.type = 'oci/slot';
              if (returnAll) {
                return this.createSlot(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createSlot(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'slotTemplate': {
              if (thisBdata.type) {
                thisBdata._type = thisBdata.type;
              }
              thisBdata.type = 'oci/slottemplate';
              if (returnAll) {
                return this.createSlotTemplate(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createSlotTemplate(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'trail': {
              if (thisBdata.type) {
                thisBdata.trailType = thisBdata.type;
              }
              thisBdata.type = 'occ/trail';
              if (returnAll) {
                return this.createTrail(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createTrail(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'trailTemplate': {
              if (thisBdata.type) {
                thisBdata.trailTemplateType = thisBdata.type;
              }
              thisBdata.type = 'occ/trailtemplate';
              if (returnAll) {
                return this.createTrailTemplate(thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.createTrailTemplate(thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            default: {
              // unsupported entity type
              const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Entity', [entity], null, null, null);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }
          }
        }
        case 'update': {
          // verify the conditionally required data within the objects was received
          if (!thisBdata) {
            const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['bodyParams'], null, null, null);
            log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
            return callback(null, errorObj);
          }
          if (!thisQdata || !thisQdata.key) {
            const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['queryParams with key'], null, null, null);
            log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
            return callback(null, errorObj);
          }

          const itemId = thisQdata.key;
          delete thisQdata.key;

          switch (entity) {
            case 'card': {
              if (thisBdata.type) {
                thisBdata.cardType = thisBdata.type;
              }
              thisBdata.type = 'oci/card';
              if (returnAll) {
                return this.updateCard(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updateCard(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'cardTemplate': {
              if (thisBdata.type) {
                thisBdata.cardTemplateType = thisBdata.type;
              }
              thisBdata.type = 'oci/cardtemplate';
              if (returnAll) {
                return this.updateCardTemplate(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updateCardTemplate(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'container': {
              if (thisBdata.type) {
                thisBdata.containerType = thisBdata.type;
              }
              thisBdata.type = 'oci/container';
              if (returnAll) {
                return this.updateContainer(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updateContainer(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'containerTemplate': {
              if (thisBdata.type) {
                thisBdata.containerTemplateType = thisBdata.type;
              }
              thisBdata.type = 'oci/containertemplate';
              if (returnAll) {
                return this.updateContainerTemplate(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updateContainerTemplate(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'port': {
              if (thisBdata.type) {
                thisBdata._type = thisBdata.type;
              }
              thisBdata.type = 'oci/port';
              if (returnAll) {
                return this.updatePort(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updatePort(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'portTemplate': {
              if (thisBdata.type) {
                thisBdata._type = thisBdata.type;
              }
              thisBdata.type = 'oci/porttemplate';
              if (returnAll) {
                return this.updatePortTemplate(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updatePortTemplate(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'shelf': {
              if (thisBdata.type) {
                thisBdata.shelfType = thisBdata.type;
              }
              thisBdata.type = 'oci/shelf';
              if (returnAll) {
                return this.updateShelf(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updateShelf(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'shelfTemplate': {
              if (thisBdata.type) {
                thisBdata.shelfTemplateType = thisBdata.type;
              }
              thisBdata.type = 'oci/shelftemplate';
              if (returnAll) {
                return this.updateShelfTemplate(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updateShelfTemplate(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'site': {
              if (thisBdata.type) {
                thisBdata.siteType = thisBdata.type;
              }
              thisBdata.type = 'oci/site';
              if (returnAll) {
                return this.updateSite(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updateSite(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'siteTemplate': {
              if (thisBdata.type) {
                thisBdata.siteTemplateType = thisBdata.type;
              }
              thisBdata.type = 'oci/sitetemplate';
              if (returnAll) {
                return this.updateSiteTemplate(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updateSiteTemplate(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'slot': {
              if (thisBdata.type) {
                thisBdata._type = thisBdata.type;
              }
              thisBdata.type = 'oci/slot';
              if (returnAll) {
                return this.updateSlot(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updateSlot(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'slotTemplate': {
              if (thisBdata.type) {
                thisBdata._type = thisBdata.type;
              }
              thisBdata.type = 'oci/slottemplate';
              if (returnAll) {
                return this.updateSlotTemplate(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updateSlotTemplate(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'trail': {
              if (thisBdata.type) {
                thisBdata.trailType = thisBdata.type;
              }
              thisBdata.type = 'occ/trail';
              if (returnAll) {
                return this.updateTrail(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updateTrail(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            case 'trailTemplate': {
              if (thisBdata.type) {
                thisBdata.trailTemplateType = thisBdata.type;
              }
              thisBdata.type = 'occ/trailtemplate';
              if (returnAll) {
                return this.updateTrailTemplate(itemId, thisBdata, thisQdata, addlHeaders, callback);
              }
              return this.updateTrailTemplate(itemId, thisBdata, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                return callback(result.response);
              });
            }
            default: {
              // unsupported entity type
              const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Entity', [entity], null, null, null);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }
          }
        }
        case 'delete': {
          // verify the conditionally required data within the objects was received
          if (!thisQdata || !thisQdata.key) {
            const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['queryParams with key'], null, null, null);
            log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
            return callback(null, errorObj);
          }

          const itemId = thisQdata.key;
          delete thisQdata.key;

          switch (entity) {
            case 'card': {
              if (returnAll) {
                return this.deleteCard(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deleteCard(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            case 'cardTemplate': {
              if (returnAll) {
                return this.deleteCardTemplate(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deleteCardTemplate(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            case 'container': {
              if (returnAll) {
                return this.deleteContainer(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deleteContainer(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            case 'containerTemplate': {
              if (returnAll) {
                return this.deleteContainerTemplate(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deleteContainerTemplate(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            case 'port': {
              if (returnAll) {
                return this.deletePort(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deletePort(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            case 'portTemplate': {
              if (returnAll) {
                return this.deletePortTemplate(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deletePortTemplate(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            case 'shelf': {
              if (returnAll) {
                return this.deleteShelf(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deleteShelf(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            case 'shelfTemplate': {
              if (returnAll) {
                return this.deleteShelfTemplate(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deleteShelfTemplate(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            case 'site': {
              if (returnAll) {
                return this.deleteSite(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deleteSite(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            case 'siteTemplate': {
              if (returnAll) {
                return this.deleteSiteTemplate(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deleteSiteTemplate(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            case 'slot': {
              if (returnAll) {
                return this.deleteSlot(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deleteSlot(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            case 'slotTemplate': {
              if (returnAll) {
                return this.deleteSlotTemplate(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deleteSlotTemplate(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            case 'trail': {
              if (returnAll) {
                return this.deleteTrail(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deleteTrail(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            case 'trailTemplate': {
              if (returnAll) {
                return this.deleteTrailTemplate(itemId, thisQdata, addlHeaders, callback);
              }
              return this.deleteTrailTemplate(itemId, thisQdata, addlHeaders, (result, error) => {
                // if we received an error or their is no response on the results return an error
                if (error) {
                  return callback(null, error.response);
                }
                // return the response
                if (result.response === 'success') {
                  return callback('success deleting');
                }
                return callback(result.response);
              });
            }
            default: {
              // unsupported entity type
              const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Entity', [entity], null, null, null);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }
          }
        }
        default: {
          // unsupported action
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Action', [action], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }
    } catch (e) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, e);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }
}

module.exports = EAI;
