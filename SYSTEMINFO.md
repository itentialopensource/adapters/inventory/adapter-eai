# EAI

Vendor: Ericsson
Homepage: https://www.ericsson.com/en

Product: EAI (Ericsson Adaptive Inventory)
Product Page: https://www.ericsson.com/en/portfolio/cloud-software-and-services/business-and-operations-support-systems/orchestration/adaptive-inventory

## Introduction
We classify EAI into the Inventory domain as EAI provides an inventory management solution that allows for visibility, automation, and optimization of network assets.

## Why Integrate
The EAI adapter from Itential is used to integrate the Itential Automation Platform (IAP) with EAI. With this adapter you have the ability to perform operations on items such as:

- Sites
- Containers
- Shelves
- Slots
- Cards
- Ports
- Paths